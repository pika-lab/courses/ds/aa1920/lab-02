task<JavaExec>("runHelloAgent") {
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "sd.lab.tucson.agents.HelloAgent"
    standardInput = System.`in`
}

task<JavaExec>("runSte") {
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "sd.lab.tucson.agents.SteAgent"
    standardInput = System.`in`
}

task<JavaExec>("runGio") {
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "sd.lab.tucson.agents.GioAgent"
    standardInput = System.`in`
}
