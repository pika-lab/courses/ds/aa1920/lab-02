package sd.lab.tucson.agents;

import alice.logictuple.LogicTuple;
import alice.logictuple.exceptions.InvalidLogicTupleException;
import alice.tucson.api.AbstractTucsonAgent;
import alice.tucson.api.ITucsonOperation;
import alice.tucson.api.SynchACC;
import alice.tucson.api.TucsonTupleCentreId;
import alice.tucson.api.exceptions.TucsonInvalidAgentIdException;
import alice.tucson.api.exceptions.TucsonInvalidTupleCentreIdException;
import alice.tucson.api.exceptions.TucsonOperationNotPossibleException;
import alice.tucson.api.exceptions.UnreachableNodeException;
import alice.tuplecentre.api.exceptions.OperationTimeOutException;
import alice.tuplecentre.core.AbstractTupleCentreOperation;

public class GioAgent extends AbstractTucsonAgent {

    public static void main(String[] args) throws TucsonInvalidAgentIdException {
        new GioAgent().go();
    }

    public GioAgent() throws TucsonInvalidAgentIdException {
        super("gio_agent");
    }

    @Override
    public void operationCompleted(AbstractTupleCentreOperation abstractTupleCentreOperation) {
    }

    @Override
    public void operationCompleted(ITucsonOperation iTucsonOperation) {
    }

    @Override
    protected void main() {
        try {
            SynchACC helloOps = getContext();
            final TucsonTupleCentreId defaultTC = new TucsonTupleCentreId("default", "localhost", "20504");
            final LogicTuple worldTuple = LogicTuple.parse("msg(ste,world)");
            final LogicTuple gioTemplate = LogicTuple.parse("msg(gio,_)");

            helloOps.in(defaultTC, gioTemplate, Long.MAX_VALUE);
            helloOps.out(defaultTC, worldTuple, Long.MAX_VALUE);

        } catch (final OperationTimeOutException | TucsonInvalidTupleCentreIdException | InvalidLogicTupleException | TucsonOperationNotPossibleException | UnreachableNodeException e) {
            e.printStackTrace();
        }
    }

}
