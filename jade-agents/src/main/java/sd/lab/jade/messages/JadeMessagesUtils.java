package sd.lab.jade.messages;

import jade.core.AID;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.UnreadableException;

import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;

public class JadeMessagesUtils {
    public static <T extends Serializable> T getPayload(final ACLMessage message) {
        try {
            return (T) message.getContentObject();
        } catch (final UnreadableException e) {
            throw new RuntimeException(e);
        }
    }

    public static <T extends Serializable> void setPayload(final ACLMessage message, final T value) {
        try {
            message.setContentObject(value);
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static ACLMessage newACLMessage(int performative, String content, AID... receivers) {
        final ACLMessage message = new ACLMessage(performative);
        Arrays.stream(receivers).forEach(message::addReceiver);
        message.setContent(content);
        return message;
    }

    public static ACLMessage newACLMessage(int performative, String content, Collection<AID> receivers) {
        final ACLMessage message = new ACLMessage(performative);
        receivers.forEach(message::addReceiver);
        message.setContent(content);
        return message;
    }

    public static ACLMessage newACLMessage(int performative, Serializable payload, AID... receivers) {
        final ACLMessage message = new ACLMessage(performative);
        Arrays.stream(receivers).forEach(message::addReceiver);
        setPayload(message, payload);
        return message;
    }

    public static ACLMessage newACLMessage(int performative, Serializable payload, Collection<AID> receivers) {
        final ACLMessage message = new ACLMessage(performative);
        receivers.forEach(message::addReceiver);
        setPayload(message, payload);
        return message;
    }

    public static ACLMessage newReply(final ACLMessage message, int performative, String content, AID... receivers) {
        final ACLMessage reply = message.createReply();
        reply.setPerformative(performative);
        Arrays.stream(receivers).forEach(reply::addReceiver);
        reply.setContent(content);
        return reply;
    }

    public static ACLMessage newReply(final ACLMessage message, int performative, String content, Collection<AID> receivers) {
        final ACLMessage reply = message.createReply();
        reply.setPerformative(performative);
        receivers.forEach(reply::addReceiver);
        reply.setContent(content);
        return reply;
    }

    public static ACLMessage newReply(final ACLMessage message, int performative, Serializable payload, AID... receivers) {
        final ACLMessage reply = message.createReply();
        reply.setPerformative(performative);
        Arrays.stream(receivers).forEach(reply::addReceiver);
        setPayload(reply, payload);
        return reply;
    }

    public static ACLMessage newReply(final ACLMessage message, int performative, Serializable payload, Collection<AID> receivers) {
        final ACLMessage reply = message.createReply();
        reply.setPerformative(performative);
        receivers.forEach(reply::addReceiver);
        setPayload(reply, payload);
        return reply;
    }

    public static AID aid(String localName, String platformName) {
        return new AID(AID.createGUID(localName, platformName), true);
    }

    public static AID aid(String localName) {
        return new AID(localName, false);
    }
}
