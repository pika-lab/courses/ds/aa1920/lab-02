package sd.lab.jade.agents;

import jade.core.Agent;
import jade.core.behaviours.Behaviour;

import java.util.Arrays;
import java.util.stream.Stream;

public abstract class BaseAgent extends Agent {

    protected void initialize(Object... args) {
        log("Hi! I'm %s and my initialisation arguments are %s", getName(), Arrays.toString(args));
    }

    @Override
    protected void setup() {
        initialize(getArguments());

        addBehaviour(mainBehaviour());
    }

    protected final void log(String format, Object... args) {
        final String logMessage = String.format(
                "[%s] " + format,
                Stream.concat(Stream.of(this.getAID().getName()), Stream.of(args)).toArray()
        );
        System.out.println(logMessage);
    }

    protected abstract Behaviour mainBehaviour();

}
