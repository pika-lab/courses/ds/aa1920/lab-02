package sd.lab.jade.agents;

import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.core.behaviours.SequentialBehaviour;
import jade.lang.acl.ACLMessage;

import static jade.lang.acl.MessageTemplate.*;
import static sd.lab.jade.messages.JadeMessagesUtils.aid;
import static sd.lab.jade.messages.JadeMessagesUtils.newACLMessage;

public class SteAgent extends BaseAgent {

    @Override
    protected Behaviour mainBehaviour() {
        final SequentialBehaviour sequence = new SequentialBehaviour();
        sequence.addSubBehaviour(sendHelloMessage());
        sequence.addSubBehaviour(waitWorldMessage());
        return sequence;
    }

    protected Behaviour sendHelloMessage() {

        final ACLMessage message = newACLMessage(
                ACLMessage.INFORM,    // performative
                "hello",      // payload
                aid("gio")  // recipient's ID
        );

        return new OneShotBehaviour() {
            @Override
            public void action() {
                log("Sending message '%s' to agent %s", message.getContent(), message.getAllReceiver().next());
                send(message);
            }
        };
    }

    protected Behaviour waitWorldMessage() {
        return new CyclicBehaviour() {
            @Override
            public void action() {
                log("Waiting to receive an INFORM message whose content is 'world'");

                final ACLMessage received = receive(
                        and(
                                MatchPerformative(ACLMessage.INFORM),
                                MatchContent("world")
                        )
                );

                if (received != null) {
                    removeBehaviour(this);
                    handleMessage(received);
                } else {
                    log("No such a message is available, I'm blocking the current behaviour");
                    this.block();
                }

            }
        };
    }

    protected void handleMessage(ACLMessage message) {
        log("I received message '%s' from %s", message.getContent(), message.getSender());
        doDelete();
        log("I'm done, goodbye.");
    }
}
