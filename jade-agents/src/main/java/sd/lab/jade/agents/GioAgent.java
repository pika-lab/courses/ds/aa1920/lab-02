package sd.lab.jade.agents;

import jade.core.AID;
import jade.core.behaviours.Behaviour;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

import static jade.lang.acl.MessageTemplate.*;
import static sd.lab.jade.messages.JadeMessagesUtils.newACLMessage;

public class GioAgent extends BaseAgent {

    @Override
    protected Behaviour mainBehaviour() {
        return waitHelloMessage();
    }

    protected Behaviour waitHelloMessage() {
        return new CyclicBehaviour() {
            @Override
            public void action() {
                log("Waiting to receive an INFORM message whose content is 'hello'");

                final ACLMessage received = receive(
                        and(
                                MatchPerformative(ACLMessage.INFORM),
                                MatchContent("hello")
                        )
                );

                if (received != null) {
                    removeBehaviour(this);
                    handleMessage(received);
                } else {
                    log("No such a message is available, I'm blocking the current behaviour");
                    this.block();
                }

            }
        };
    }

    protected void handleMessage(ACLMessage message) {
        log("I received message '%s' from %s", message.getContent(), message.getSender());

        addBehaviour(
                sendWorldMessage(message.getSender())
        );
    }

    protected Behaviour sendWorldMessage(AID answerToAid) {

        final ACLMessage message = newACLMessage(
                ACLMessage.INFORM,    // performative
                "world",       // payload
                answerToAid           // recipient's ID
        );

        return new OneShotBehaviour() {
            @Override
            public void action() {
                log("Sending message '%s' to agent %s", message.getContent(), message.getAllReceiver().next());

                send(message);

                doDelete();
                log("I'm done, goodbye.");
            }
        };
    }
}
