repositories {
    maven("https://jade.tilab.com/maven")
}

dependencies {
    implementation("com.tilab.jade", "jade", "4.5.0")
}

fun ifPropertyIsSet(propertyName: String, action: (String)->Unit) {
    if (project.hasProperty(propertyName)) {
        project.property(propertyName).let {
            if (it !== null && it is String && !it.isBlank()) {
                action(it)
            }
        }
    }
}

task<JavaExec>("jadePlatform") {
    group = "jade"

    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "jade.Boot"
    standardInput = System.`in`
    args("-gui", "-name", project.property("platformName"))

    ifPropertyIsSet("mainHost") {
        args("-local-host", it)
    }

    ifPropertyIsSet("mainPort") {
        args("-local-port", it)
    }

    ifPropertyIsSet("agents") {
        args("-agents", it)
    }

    doFirst {
        println(commandLine.joinToString(" "))
    }
}

fun setupContainerTask(task: JavaExec) {
    with(task) {
        group = "jade"

        sourceSets {
            main {
                classpath = runtimeClasspath
            }
        }
        main = "jade.Boot"
        standardInput = System.`in`
        args("-container")

        ifPropertyIsSet("noSearch") { noSearch ->
            if (noSearch.toBoolean()) {
                ifPropertyIsSet("mainHost") { mainHost ->
                    args("-local-host", mainHost)
                }
            }
        }

        ifPropertyIsSet("mainPort") {
            args("-local-port", it)
        }

        doFirst {
            println(commandLine.joinToString(" "))
        }
    }
}

task<JavaExec>("jadeContainer") {

    setupContainerTask(this)

    ifPropertyIsSet("agents") {
        args("-agents", it)
    }

}

task<JavaExec>("jadeContainerForGio") {

    setupContainerTask(this)

    args("-agents", "gio:sd.lab.jade.agents.GioAgent()")

}

task<JavaExec>("jadeContainerForSte") {

    setupContainerTask(this)

    args("-agents", "ste:sd.lab.jade.agents.SteAgent()")

}