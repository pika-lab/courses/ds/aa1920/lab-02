plugins {
    java
}

repositories {
    mavenCentral()
}

group = "sd.lab"
version = "1.0-SNAPSHOT"

val tucsonJars = listOf("2p", "tucson").map { "${rootDir}/libs/$it.jar" }.toTypedArray()

subprojects {

    repositories {
        mavenCentral()
    }

    apply<JavaPlugin>()

    dependencies {
        implementation(files(*tucsonJars))

        testImplementation("junit", "junit", "4.12")
    }

    configure<JavaPluginConvention> {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}

dependencies {
    implementation(files(*tucsonJars))
}

task<JavaExec>("tucson") {
    dependsOn("classes")
    group = "tucson"
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "alice.tucson.service.TucsonNodeService"
    standardInput = System.`in`

    if ("port" in properties) {
        args = listOf("-portno", properties["port"].toString())
    }
}

task<JavaExec>("inspector") {
    dependsOn("classes")
    group = "tucson"
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "alice.tucson.introspection.tools.InspectorGUI"
    standardInput = System.`in`

    if ("port" in properties) {
        args = listOf("-portno", properties["port"].toString())
    }
}

task<JavaExec>("cli") {
    dependsOn("classes")
    group = "tucson"
    sourceSets {
        main {
            classpath = runtimeClasspath
        }
    }
    main = "alice.tucson.service.tools.CommandLineInterpreter"
    standardInput = System.`in`

    if ("port" in properties) {
        args = listOf("-portno", properties["port"].toString())
    }
}
